This is a starter template for [Ionic](http://ionicframework.com/docs/) projects.

## How to use this template

*This template does not work on its own*. The shared files for each starter are found in the [ionic2-app-base repo](https://github.com/ionic-team/ionic2-app-base).

To use this template, either create a new ionic project using the ionic node.js utility, or copy the files from this repository into the [Starter App Base](https://github.com/ionic-team/ionic2-app-base).

### With the Ionic CLI:

Take the name after `ionic2-starter-`, and that is the name of the template to be used when using the `ionic start` command below:

```bash
$ sudo npm install -g ionic cordova
$ ionic start myBlank blank
```

Then, to run it, cd into `myBlank` and run:

```bash
$ ionic cordova platform add ios
$ ionic cordova run ios
```

Substitute ios for android if not on a Mac.

### Basic Partner Config 

Banner URL
https://s3-eu-west-1.amazonaws.com/townapps2017/images/townapps-images2.jpg

Stay
https://s3-eu-west-1.amazonaws.com/townapps2017/images/ic_local_hotel_black_48dp_1x.png

Eat
https://s3-eu-west-1.amazonaws.com/townapps2017/images/ic_restaurant_black_48dp_1x.png

Drink
https://s3-eu-west-1.amazonaws.com/townapps2017/images/ic_local_cafe_black_48dp_1x.png

Shop
https://s3-eu-west-1.amazonaws.com/townapps2017/images/ic_local_grocery_store_black_48dp_1x.png
