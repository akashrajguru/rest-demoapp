import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-businesslistings',
  templateUrl: 'businesslistings.html',
})
export class BusinesslistingsPage implements OnInit {

  Blistings: any;
  index: number;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ngOnInit() {
    this.Blistings = this.navParams.get('BList');
    console.log('Blist', this.Blistings);
  }

}
