import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GetdataProvider } from '../../providers/getdata/getdata';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http'; 

import { BusinesslistingsPage } from "../businesslistings/businesslistings";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  private items: any; 
  public pois = [];
  // public app_info = [];
  public blistings = []


  constructor(public navCtrl: NavController, public navParams: NavParams,  public http: Http) {
  }

  ionViewDidLoad(){
    
   //   this.http.get('http://localhost:3000/v1/poi_dp').map(res => res.json()).subscribe(data => {
   //    this.items=data; 
   //    console.log(this.items);
   //  });
   this.http.get('http://192.168.200.113:3002/api/v1/sync/0').subscribe(data =>{
     //console.log('data: ',data['_body']);

     this.items = JSON.parse(data['_body']);
     // this.items = Array.of(this.items.pois);
     //this.items = ['one', 'two', 'three'];
     // this.items = JSON.parse(data['_body']).results;
      this.pois = this.items.result.categories;
      this.blistings = this.items.result.listings;
      
    //  this.app_info.push(this.items.service_info);
    //     console.log('items1 : ',this.app_info);
    //    let j =1;
    //   for(let i in this.items.pois){
    //     console.log(j)
    //      j++;
 
    //    //  console.log(this.items[i]);
    //   this.pois.push(this.items.pois[i]);
    //   }
      console.log('items : ',this.pois);
   },error=>{console.log(error);// Error getting the data
   });
   
   } 

   onLoadBusinessListing(index: number) {
     this.navCtrl.push(BusinesslistingsPage, {BList: this.blistings, index: index});
   }

}
