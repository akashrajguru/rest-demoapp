import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { HomePage } from "../home/home";
import { LocalweatherPage } from "../localweather/localweather";
import { OffersPage } from "../offers/offers";


@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

   homePage = HomePage;
   localWeatherPage = LocalweatherPage;
   offersPage = OffersPage;

}
