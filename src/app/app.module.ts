import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { GetdataProvider } from '../providers/getdata/getdata';
import { HttpModule } from '@angular/http';

import { TabsPage } from "../pages/tabs/tabs";
import { LocalweatherPage } from "../pages/localweather/localweather";
import { OffersPage } from "../pages/offers/offers";
import { BusinesslistingsPage } from "../pages/businesslistings/businesslistings";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    LocalweatherPage,
    OffersPage,
    BusinesslistingsPage 
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    LocalweatherPage,
    OffersPage,
    BusinesslistingsPage  
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GetdataProvider
  ]
})
export class AppModule {}
